from rest_framework import serializers
from hospApp.models.paciente import Paciente
from hospApp.models.medico import Medico

class PacienteCambiarMedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = ['medico']