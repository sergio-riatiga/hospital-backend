import imp
from rest_framework import generics, status, views
from rest_framework.response import Response

from hospApp.models.paciente import Paciente
from hospApp.serializers.pacienteCambiarMedicoSerializer import PacienteCambiarMedicoSerializer

class PacienteCambiarMedicoView(generics.UpdateAPIView):
    queryset = Paciente.objects.all()
    serializer_class = PacienteCambiarMedicoSerializer