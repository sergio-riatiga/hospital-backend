from rest_framework import serializers

from hospApp.models.signoVital import SignoVital

class SignoVitalSerializer(serializers.ModelSerializer):
    class Meta:
        model = SignoVital
        fields = ['oximetria', 'frecuenciaRespiratoria', 'frecuenciaCardiaca', 'temperatura', 'presionArterial', 'glicemia', 'paciente']