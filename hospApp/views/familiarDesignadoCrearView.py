from rest_framework import status, views
from rest_framework.response import Response

from hospApp.serializers.familiarDesignadoSerializer import FamiliarDesignadoSerializer

class FamiliarDesignadoCrearView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = FamiliarDesignadoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"estado": "familiar designado creado"}, status=status.HTTP_201_CREATED)