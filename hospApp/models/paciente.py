from django.db import models

from .persona import Persona
from .medico import Medico

class Paciente(Persona):
    direccion = models.CharField(max_length=150)
    ciudad = models.CharField(max_length=150)
    fechaNacimiento = models.DateField(max_length=150)
    latitud = models.CharField(max_length=150)
    longitud = models.CharField(max_length=150)
    medico = models.ForeignKey(
        Medico,
        on_delete=models.CASCADE,
        unique=False,
        blank=True,
        null=True
    )