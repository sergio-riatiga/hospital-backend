from rest_framework import status, views
from rest_framework.response import Response

from hospApp.serializers.medicoSerializer import MedicoSerializer

class MedicoCrearView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = MedicoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"estado": "medico creado"}, status=status.HTTP_201_CREATED)