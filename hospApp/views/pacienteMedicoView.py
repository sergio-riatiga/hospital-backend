from rest_framework import generics, status, views
from rest_framework.response import Response

from hospApp.models.paciente import Paciente
from hospApp.serializers.pacienteSerializer import PacienteSerializer

class PacienteMedicoView(generics.ListAPIView):
    serializer_class = PacienteSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        idmedico = self.kwargs['medico']
        return Paciente.objects.filter(medico = idmedico)